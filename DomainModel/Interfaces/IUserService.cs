using System;
using System.Collections.Generic;
using System.Text;
using Rockport.InvestorPortal.DomainModel.Models;

namespace Rockport.InvestorPortal.DomainModel.Interfaces
{
    public interface IUserService
    {
        User GetUserById(string id);
    }
}