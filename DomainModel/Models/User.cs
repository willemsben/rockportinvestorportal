using System;
using System.Collections.Generic;
using System.Text;

namespace Rockport.InvestorPortal.DomainModel.Models
{
    public class User
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
    }
}