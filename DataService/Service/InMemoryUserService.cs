using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using Rockport.InvestorPortal.DomainModel.Interfaces;
using Rockport.InvestorPortal.DomainModel.Models;

namespace Rockport.InvestorPortal.DataService.Service
{
    public class InMemoryUserService : IUserService
    {
        private ConcurrentDictionary<string, User> _users = new ConcurrentDictionary<string, User>();

        public InMemoryUserService()
        {
            Seed();
        }

        private void Seed()
        {
            AddUser("Default User", "mypassword");
        }

        private void AddUser(string name, string password)
        {
            var id = Guid.NewGuid().ToString();
            var user = new User()
            {
                Id = id,
                DisplayName = name
            };

            _users.AddOrUpdate(id, user, (existingId, existingUser) =>
            {
                return user;
            });
        }

        public User GetUserById(string id)
        {
            throw new NotImplementedException();
        }
    }
}