$env:SolutionBaseName = "Rockport.InvestorPortal"

Write-Host "dotnet version: $(dotnet --version)"

Write-Host "+--------------------------------------------------------+" -back green -fore black
Write-Host "| Projects At Start                                      |" -back green -fore black
Write-Host "+--------------------------------------------------------+" -back green -fore black
dotnet sln list


Write-Host "+--------------------------------------------------------+" -back green -fore black
Write-Host "| Remove project references...            |" -back green -fore black
Write-Host "+--------------------------------------------------------+" -back green -fore black
dotnet remove .\ClientService\ClientService.csproj reference package Yahara.Core.Web\Yahara.Core.Web.csproj
dotnet remove .\Website.Core\Website.Core.csproj   reference package Yahara.Core.Web\Yahara.Core.Web.csproj
dotnet remove .\Website.Core\Website.Core.csproj   reference package Yahara.Core.Web.AspNetCore\Yahara.Core.Web.AspNetCore.csproj

Write-Host "+--------------------------------------------------------+" -back green -fore black
Write-Host "| Removing projects...                                   |" -back green -fore black
Write-Host "+--------------------------------------------------------+" -back green -fore black
dotnet sln .\WebCore.sln remove Website.ASP.NET\Website.ASP.NET.csproj `
                                Yahara.Core.Web\Yahara.Core.Web.csproj `
                                Yahara.Core.Web.AspNET\Yahara.Core.Web.AspNET.csproj `
                                Yahara.Core.Web.AspNetCore\Yahara.Core.Web.AspNetCore.csproj


Write-Host "+--------------------------------------------------------+" -back green -fore black
Write-Host "| Projects after cleanup...                              |" -back green -fore black
Write-Host "+--------------------------------------------------------+" -back green -fore black
dotnet sln list


Write-Host "+--------------------------------------------------------+" -back green -fore black
Write-Host "| Remove extra directories...                            |" -back green -fore black
Write-Host "+--------------------------------------------------------+" -back green -fore black
$subdirsToDelete = `
    "./Website.ASP.NET", `
    "./Yahara.Core.Web", `
    "./Yahara.Core.Web.AspNET", `
    "./Yahara.Core.Web.AspNetCore", `
    "./ClientService/submodules", `
    "./ClientService/node_modules"

foreach($subdir in $subdirsToDelete) {
    if (Test-Path $subdir) {
        Remove-Item -Recurse -Force $subdir
    }
    else {
        Write-Host "Already removed $($subdir)"
    }
}

Write-Host "+--------------------------------------------------------+" -back green -fore black
Write-Host "| Remove extra appveyor configs...                       |" -back green -fore black
Write-Host "+--------------------------------------------------------+" -back green -fore black
Get-ChildItem . -Include *.appveyor.yml -recurse | ForEach-Object {
    $baseFileName = Split-Path $_ -leaf;
    if ($baseFileName -ne "website.core.appveyor.yml") {
        Write-Host "Deleting $($_)"
        Remove-Item -Force $_
    }
}

Write-Host "+--------------------------------------------------------+" -back green -fore black
Write-Host "| Add project references to nuget packages...            |" -back green -fore black
Write-Host "+--------------------------------------------------------+" -back green -fore black
dotnet add .\ClientService\ClientService.csproj package Yahara.Core.Web
dotnet add .\Website.Core\Website.Core.csproj   package Yahara.Core.Web
dotnet add .\Website.Core\Website.Core.csproj   package Yahara.Core.Web.AspNetCore


Write-Host "+--------------------------------------------------------+" -back green -fore black
Write-Host "| Replace solution base name...                          |" -back green -fore black
Write-Host "+--------------------------------------------------------+" -back green -fore black
Get-ChildItem . -Include *.cs,*.csproj,*.cshtml -recurse | ForEach-Object {
    $c = ($_ | Get-Content) 
    $c = $c -replace 'Yahara.WebCore.Example', "$($env:SolutionBaseName)"
    [IO.File]::WriteAllText($_.FullName, ($c -join "`r`n"))
}


Write-Host "+--------------------------------------------------------+" -back green -fore black
Write-Host "| Install yarn packages...                               |" -back green -fore black
Write-Host "+--------------------------------------------------------+" -back green -fore black
cd "ClientService"
yarn add @yahara/logging @yahara/react-spa @yahara/react-spa-builder -W
cd "../"


Write-Host "+--------------------------------------------------------+" -back green -fore black
Write-Host "| Rename solution file...                                |" -back green -fore black
Write-Host "+--------------------------------------------------------+" -back green -fore black
Rename-Item -Path .\WebCore.sln -NewName "$($env:SolutionBaseName).sln"