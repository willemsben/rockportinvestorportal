# Get New Version
param (
    [Parameter(Mandatory=$true)][string]$version
)

Write-Host "+--------------------------------------------------------+" -back green -fore black
Write-Host "+Updating .csproj Package Versions                       +" -back green -fore black
Write-Host "+--------------------------------------------------------+" -back green -fore black

Write-Host "Targeting: $($version)"

Get-ChildItem . -Include *.csproj -recurse | ForEach-Object {
    Write-Host "--------------------------------------------------------"
    Write-Host "Updating $($_)"
    [xml]$xml = (Get-Content $_);
    $properties = $xml.Project["PropertyGroup"];
    
    $propertyNames = `
        "Version", `
        "FileVersion", `
        "AssemblyVersion"

    foreach($nodeName in $propertyNames) {
        $node = $properties[$nodeName];

        Write-Host "Tag: $($nodeName)"
        
        if ($node) {
            Write-Host "                     : Existing := $($node.InnerText)"
            $node.InnerText = $version
            Write-Host "                     : Replaced := $($node.InnerText)"
        }
        else {
            Write-Host "                     : $($nodeName) not found in $($properties.Name)"
        }
        
    }

    $xml.Save($_);
    Write-Host " "
}

Write-Host "--------------------------------------------------------"

Write-Host "Done"
