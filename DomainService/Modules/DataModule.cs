using Autofac;
using System;
using System.Collections.Generic;
using System.Text;
using Rockport.InvestorPortal.DataService.Service;
using Rockport.InvestorPortal.DomainModel.Interfaces;

namespace Rockport.InvestorPortal.DomainService.Modules
{
    public class DataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<InMemoryUserService>().As<IUserService>();
        }
    }
}