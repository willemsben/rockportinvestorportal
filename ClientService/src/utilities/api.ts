export type IFetcher = typeof fetch;

export interface IApiOptions {
    baseUrl: string;
    wrappedFetch: IFetcher;
}