import { React } from "@yahara/react-spa/dist/externals";
import { bind } from "bind-decorator";
import { Link } from "react-router-dom";
import {
    Route,
    RouteComponentProps
} from "react-router";
import { componentTemplate } from "./templates/newComponent";
import { componentStylesheetTemplate } from "./templates/newComponentStyle";

const path = require('path');

const styles = require('./ComponentTemplate.scss') as {
    main: string;
}

interface IComponentTemplatePageProps {

}

interface IComponentTemplatePageState {
    componentName: string;
}

export class ComponentTemplatePage extends React.Component<IComponentTemplatePageProps, IComponentTemplatePageState> {
    state = {
        componentName: 'NewComponent'
    }

    @bind
    updateComponentName(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            componentName: e.target.value
        })
    }

    render() {
        return (
            <div className={styles.main}>
                <h2>New Component Template</h2>
                <p>
                    This utility will give you a quick way to create new components.  Usually, a component will
                    consist of both a stylesheet (.scss) file and a typescript (.tsx) file.
                </p>

                <label>Component Name:</label>
                <input
                    value={this.state.componentName}
                    onChange={this.updateComponentName}
                />

                <h3>{this.state.componentName}.tsx</h3>
                <pre>
                    <code>
                        {componentTemplate(this.state.componentName)}
                    </code>
                </pre>

                <h3>{this.state.componentName}.scss</h3>
                <pre>
                    <code>
                        {componentStylesheetTemplate(this.state.componentName)}
                    </code>
                </pre>
            </div>
        )
    }
}
