import { React } from "@yahara/react-spa/dist/externals";
import { bind } from "bind-decorator";

interface IDevelopmentProps {

}

interface IDevelopmentState {
    
}

export class Development extends React.Component<IDevelopmentProps, IDevelopmentState> {
    state = {
        
    }

    render() {
        return (
            <div>
                <h2>Development</h2>
            </div>
        )
    }
}