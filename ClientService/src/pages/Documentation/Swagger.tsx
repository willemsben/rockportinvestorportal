import { React } from "@yahara/react-spa/dist/externals";
import { bind } from "bind-decorator";
import { Button } from "material-ui";
import { Terminal } from "../../components/Terminal";
import { IDocumentationProps } from "./documentationPages";
import { ValuesApiFactory } from "../../generated/api";

const styles = require('./Swagger.scss') as {
    code: string;
    inset: string;
    main: string;
    table: string;
}


interface ISwaggerState {
    values: string[] | null;
}

export class Swagger extends React.Component<IDocumentationProps, ISwaggerState> {
    private valuesApi = ValuesApiFactory(this.props.apiOptions.wrappedFetch, this.props.apiOptions.baseUrl);

    state: ISwaggerState = {
        values: null
    }

    @bind
    clear() {
        this.setState({values: null});
    }

    @bind
    async loadValues() {
        let values = await this.valuesApi.apiValuesGet();

        this.setState({values});
    }

    render() {
        return (
            <div className={styles.main}>
                <h2>Swashbuckle/API</h2>

                <p>
                    Swashbuckle allows us to compile a typescript API that we can
                    use to communicate with the back end server.  It also allows
                    us to use Swagger UI, which is a useful way to explore the API.
                </p>

                <Button
                    variant="raised"
                    color="primary"
                    onClick={e => window.location.href = "/swagger"}
                >
                    Swagger UI
                </Button>
                <span> </span>
                <Button
                    variant="raised"
                    color="primary"
                    onClick={e => window.location.href = "/swagger/v1/swagger.json"}
                >
                    Swagger Definition
                </Button>

                <p>
                    You can test this api by using the button below to load the values:
                </p>

                <Button
                    variant="raised"
                    color="primary"
                    onClick={this.loadValues}
                >
                    Load Values
                </Button>
                <span> </span>
                <Button
                    variant="raised"
                    color="primary"
                    onClick={this.clear}
                >
                    Clear Values
                </Button>

                
                {this.state.values ? (
                    <ul>
                        {this.state.values.map((text, i) => <li key={i}>{text}</li>)}
                    </ul>
                ) : (
                    <ul>
                        <li>(Values not loaded)</li>
                    </ul>
                )}

                <p>
                    Although not documented here, you can also use the swagger
                    definition file to generate PDF documentation, which you
                    can send to clients or consumers of the API.  You can also
                    generate clients in languages other than typescript, or
                    generate stubs for a server, for instance, if you want to
                    mock or re-implement the API in another language or in a
                    different project.
                </p>

                <h2>Regenerate the API</h2>

                <p>
                    You'll need to regenerate the API every time you change the signatures of the
                    API for the application
                </p>

                <p>
                    Create the following directories, if they don't already exist:
                </p>

                <ul>
                    <li>C:\tmp\input\</li>
                    <li>C:\tmp\output\</li>
                </ul>

                <p>
                    Now, copy and paste the definition file to <strong>C:\tmp\input\swagger.json</strong>.
                </p>

                <p>
                    Run the following command:
                </p>

                <div className={styles.inset}>
                    <Terminal commands={[
                        {
                            pwd: './ClientService',
                            command: 'npm run-script generate-api',
                            output: (colors) => {
                                return (
                                    <span>
                                        {[
                                            '',
                                            '> react-spa-template@0.1.0 generate-api C:\Users\Jon\Desktop\GitHub\yahara.core.web\ClientService',
                                            '> docker run -v C:\tmp\input\:/INPUT -v C:\tmp\output\:/OUTPUT -t --rm jonathonrichardson/swagger-codegen java -jar modules/swagger-codegen-cli/target/swagger-codegen-cli.jar generate -i /INPUT/swagger.json -l typescript-fetch -o /OUTPUT/generated-ts -DmodelPropertyNaming=original',
                                            '',
                                            '[main] INFO io.swagger.parser.Swagger20Parser - reading from /INPUT/swagger.json',
                                            '[main] INFO io.swagger.codegen.AbstractGenerator - writing file /OUTPUT/generated-ts/api.ts',
                                            '[main] INFO io.swagger.codegen.AbstractGenerator - writing file /OUTPUT/generated-ts/git_push.sh',
                                            '[main] INFO io.swagger.codegen.DefaultGenerator - writing file /OUTPUT/generated-ts/README.md',
                                            '[main] INFO io.swagger.codegen.AbstractGenerator - writing file /OUTPUT/generated-ts/package.json',
                                            '[main] INFO io.swagger.codegen.AbstractGenerator - writing file /OUTPUT/generated-ts/typings.json',
                                            '[main] INFO io.swagger.codegen.AbstractGenerator - writing file /OUTPUT/generated-ts/tsconfig.json',
                                            '[main] INFO io.swagger.codegen.AbstractGenerator - writing file /OUTPUT/generated-ts/tslint.json',
                                            '[main] INFO io.swagger.codegen.DefaultGenerator - writing file /OUTPUT/generated-ts/.gitignore',
                                            '[main] INFO io.swagger.codegen.AbstractGenerator - writing file /OUTPUT/generated-ts/.swagger-codegen/VERSION'
                                        ].map((text, i) => <span key={i}>{text}<br/></span>)}
                                    </span>
                                )
                            }
                        },
                    ]}/>
                </div>

                <p>
                    The output will be in <strong>C:\tmp\output\api.ts</strong>.  You'll need to replace
                    the following two lines in the output:
                </p>

                <pre>
import * as isomorphicFetch from "isomorphic-fetch";
<br/>
import * as assign from "core-js/library/fn/object/assign";
                </pre>

                <p> with... </p>
                <pre>
const isomorphicFetch = fetch;
<br/>
const assign: (target: any, ...sources: any[]) => any = (Object as any).assign;
                </pre>

                <p>
                    Now, finally, copy and paste that result to <strong>/src/generated/api.ts</strong>
                </p>
            </div>
        )
    }
}