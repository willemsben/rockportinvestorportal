import { React } from "@yahara/react-spa/dist/externals";
import { bind } from "bind-decorator";

interface IIntroductionProps {

}

interface IIntroductionState {
    
}

export class Introduction extends React.Component<IIntroductionProps, IIntroductionState> {
    state = {
        
    }

    render() {
        return (
            <div>
                <h2>Introduction</h2>
                <p>TODO: Write an introduction.</p>
            </div>
        )
    }
}