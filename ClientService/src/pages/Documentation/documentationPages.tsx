import * as React from "react";
import { Introduction } from "./Introduction";
import { ProjectsPage } from "./Projects";
import { ComponentTemplatePage } from "./ComponentTemplate";
import { Technologies } from "./Technology";
import { Packages } from "./Packages";
import { Development } from "./Development";
import { ReleaseNotes } from "./ReleaseNotes";
import { MaterialUI } from "./Material-UI";
import { Swagger } from "./Swagger";
import { IApiOptions } from "../../utilities/api";

export interface IDocumentationProps {
    baseUrl: string;
    apiOptions: IApiOptions;
}

type DocumentationPageConstructor = (props: IDocumentationProps) => JSX.Element;

const DocumentationPages: {[name: string]: DocumentationPageConstructor} = {
    'Introduction': (props) => <Introduction />,
    'Technologies': (props) => <Technologies />,
    'Project List': (props) => <ProjectsPage />,
    'Javascript Packages': (props) => <Packages />,
    'New Component Template': (props) => <ComponentTemplatePage />,
    'Release Notes': (props) => <ReleaseNotes />,
    'Development': (props) => <Development />,
    'Material UI': (props) => <MaterialUI />,
    'Swagger': (props) => <Swagger 
        baseUrl={props.baseUrl}
        apiOptions={props.apiOptions}
    />,
};

export { DocumentationPages };

export type DocumentPage = keyof typeof DocumentationPages;