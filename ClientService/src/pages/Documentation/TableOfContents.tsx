import { React } from "@yahara/react-spa/dist/externals";
import { bind } from "bind-decorator";
import { NavLink } from "react-router-dom";
import {
    RouteComponentProps,
    Route
} from "react-router";
import * as _ from "lodash";
import { DocumentationPages } from './documentationPages';

const path = require('path');

const styles = require('./TableOfContents.scss') as {
    active: string;
    link: string;
    main: string;
}

interface ITableOfContentsProps {
    basePath: string;
}

interface ITableOfContentsState {
    
}

export class TableOfContents extends React.Component<ITableOfContentsProps, ITableOfContentsState> {
    state = {
        
    }

    @bind
    mkLink(address: string, text?: string) {
        if (!text) {
            text = address;
        }

        let url = path.join(this.props.basePath, address)

        return (
            <NavLink
                key={address}
                to={url}
                activeClassName={styles.active}
                className={styles.link}
            >
                {text}
            </NavLink>
        )
    }

    @bind
    route(address: string, render: (props: RouteComponentProps<any>) => JSX.Element) {
        let url = path.join(this.props.basePath, address);
        return (
            <Route path={url} render={render} />
        )
    }

    render() {
        return (
            <div className={styles.main}>
                {_.map(DocumentationPages, (val, key) => {
                    return this.mkLink(key);
                })}
            </div>
        )
    }
}