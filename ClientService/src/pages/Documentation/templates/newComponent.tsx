export const componentTemplate = (componentName: string) => {
    return `
import { React } from "@yahara/react-spa/dist/externals";
import { bind } from "bind-decorator";

const styles = require('./${componentName}.scss') as {
    main: string;
}

interface I${componentName}Props {

}

interface I${componentName}State {
    
}

export class ${componentName} extends React.Component<I${componentName}Props, I${componentName}State> {
    state: I${componentName}State = {
        
    }

    render() {
        return (
            <div className={styles.main}>
            </div>
        )
    }
}
`;
};