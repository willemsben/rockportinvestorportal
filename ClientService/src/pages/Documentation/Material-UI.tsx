import { React } from "@yahara/react-spa/dist/externals";
import { bind } from "bind-decorator";
import {
    Button
} from "material-ui";

const styles = require('./Material-UI.scss') as {
    code: string;
    inset: string;
    main: string;
    table: string;
};

const links = {
    breakingChanges: 'https://github.com/mui-org/material-ui/blob/v1-beta/ROADMAP.md#breaking-changes-before-v1'
}

interface IMaterialUIProps {

}

interface IMaterialUIState {
    
}

export class MaterialUI extends React.Component<IMaterialUIProps, IMaterialUIState> {
    state = {
        
    }

    render() {
        return (
            <div className={styles.main}>
                <h2>Material UI</h2>

                <p>
                    The template project includes material UI by default.  Right now, it uses a pre-release
                    of v1.  This works for now, but you may want to upgrade eventually.  <a href={links.breakingChanges}>
                    This page</a> lists all of the potential breaking changes between the current version
                    and the final release of v1, in case you want to/need to upgrade.
                </p>

                <Button variant="raised" color="primary">Example Button</Button>
            </div>
        )
    }
}