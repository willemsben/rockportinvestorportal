import { React } from "@yahara/react-spa/dist/externals";
import { bind } from "bind-decorator";

const styles = require('./Packages.scss') as {
    code: string;
    inset: string;
    main: string;
    table: string;
}

const links = {
    timeBestPractices: "https://stackoverflow.com/a/2532962/4339894",
    selVerResolutions: "https://yarnpkg.com/lang/en/docs/selective-version-resolutions/",
    workspaces: "https://yarnpkg.com/lang/en/docs/workspaces/",
    workspacesBlog: "https://yarnpkg.com/blog/2017/08/02/introducing-workspaces/",
};

interface IPackagesProps {

}

interface IPackagesState {
    
}

export class Packages extends React.Component<IPackagesProps, IPackagesState> {
    state = {
        
    }

    render() {
        return (
            <div className={styles.main}>
                <h2>Javascript Packages</h2>

                <p>
                    Here is a list of the packages included in the template project:
                </p>


                <div className={styles.inset}>
                    <table className={styles.table}>
                        <thead>
                            <tr>
                                <th>Package Name</th>
                                <th>Description</th>
                                <th>Notes</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><strong>Moment</strong></td>
                                <td>
                                    Time and timezone utilities.  Note that in most cases, you should be
                                    storing times as UTC codes (there 
                                    are <a href={links.timeBestPractices}>some exceptions</a>), and 
                                    using <Code>moment</Code> to format that date according to the 
                                    user's local (or configured) time zone.
                                    <br/><br/>
                                    Look in the future for some default handling of storing/accessing
                                    a user's preferred timezone.
                                </td>
                                <td>
                                    Includes <Code>moment-timezone</Code> and <Code>@types/moment-timezone</Code>.
                                    Note that <Code>moment</Code> itself doesn't need a <strong>@types</strong> package,
                                    because it includes the types in the main package.
                                    <br/><br/>
                                    Note that if you need to update any of these packages, you should update all of
                                    them in tandem.
                                </td>
                            </tr>
                            <tr>
                                <td><strong>React Router</strong></td>
                                <td>
                                    Used for handling routing on the page.
                                </td>
                                <td>
                                    Includes <Code>react-router</Code> and <Code>react-router-dom</Code>,
                                    as well as the appropriate <strong>@types</strong> packages.  
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Lodash</strong></td>
                                <td>
                                    A basic utility library.  Basically the same thing as it's
                                    predecessor: <strong>Underscore.js</strong>
                                    <br/><br/>
                                    Used for basic and common repeated simple array and object handling,
                                    as well as checking for <Code>undefined</Code> and detecting object
                                    types such as <Code>string</Code> and <Code>object</Code>.
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h2>@yahara Packages</h2>

                <p>
                    Yahara provides a couple of packages as well:
                </p>

                
                <div className={styles.inset}>
                    <table className={styles.table}>
                        <thead>
                            <tr>
                                <th>Package Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><strong>logging</strong></td>
                                <td>
                                    Provides a base logging library.
                                </td>
                            </tr>
                            <tr>
                                <td><strong>react-spa</strong></td>
                                <td>
                                    Provides the runtime handling/mounting of the application, as
                                    well as some helpful props to the application to handle updating
                                    the favicon, page title, etc.
                                </td>
                            </tr>
                            <tr>
                                <td><strong>react-spa-builder</strong></td>
                                <td>
                                    Provides a base webpack config with the following:
                                    <ul>
                                        <li>ES5 Polyfill</li>
                                        <li>Webpack Dev Server</li>
                                        <li>HMR</li>
                                        <li>React Hot Loader</li>
                                        <li>
                                            Globals through the Define Plugin
                                            <ul>
                                                <li>process.env.NODE_ENV</li>
                                                <li>VERSION</li>
                                                <li>COMMITHASH</li>
                                                <li>BRANCH</li>
                                                <li>VERSION_WITH_TAG</li>
                                                <li>PRODUCTION_MODE</li>
                                            </ul>
                                        </li>
                                        <li>NoEmitOnErrorsPlugin is used to force webpack to fail and not emit file when it fails</li>
                                        <li>NamedModulesPlugin used to make readable module names</li>
                                        <li>
                                            SASS/SCSS/CSS Handling
                                            <ul>
                                                <li>CSS modules are </li>
                                                <li>
                                                    In production mode, all styles are written 
                                                    to <Code>bundle.css</Code>, to prevent a flash
                                                    of unstyled content when loading the page.
                                                </li>
                                                <li>Automatic vendor prefixing.</li>
                                            </ul>
                                        </li>
                                        <li>Source maps for CSS/SCSS/TS/JS/TSX files</li>
                                        <li>
                                            File loader configured to load jpg, jpeg, png, gif, svg, woff, woff2, eot, ttf, and ico files
                                            as assets automatically.
                                        </li>
                                        <li>React Tap Event Plugin</li>
                                        <li>Typescript Support</li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h2>Yarn</h2>

                <p>
                    We use <strong>Yarn</strong> instead of <strong>npm</strong> for several reasons:
                    <ul>
                        <li>Consistent builds</li>
                        <li>Better Build Performance</li>
                        <li><a href={links.selVerResolutions}>Selective Version Resolutions</a> (<a href={links.workspacesBlog}>Blog Post</a>)</li>
                        <li><a href={links.workspaces}>Support for workspaces</a></li>
                    </ul>
                </p>

                <h3>Selective Version Resolutions</h3>

                <p>
                    Yarn has a really cool feature in it's support for selective version resolutions.
                    This feature allows you to override how yarn resolves various packages.  Why would
                    you want to do this?  Certain dependencies, like <Code>@types</Code> packages and
                    <Code>React</Code> have issues when you load multiple versions of them into a project.
                    Using selective version resolutions allows you to overcome small variations in the
                    requested version of some of these pacakges.
                </p>

                
                <div className={styles.inset}>
                    <table className={styles.table}>
                        <thead>
                            <tr>
                                <th>Version</th>
                                <th>Resolutions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>&lt;v1.3</td>
                                <td>
                                    Provides a base logging library.
                                </td>
                            </tr>
                            <tr>
                                <td>v1.3</td>
                                <td>
                                    <pre> {JSON.stringify({
                                        "resolutions": {
                                            "**/@types/react": "16.0.38",
                                            "**/@types/react-dom": "16.0.4",
                                            "**/@types/react-router": "4.0.22",
                                            "**/@types/react-tap-event-plugin": "0.0.30",
                                            "**/react-tap-event-plugin": "3.0.2",
                                            "**/react-router": "4.2.0",
                                            "**/react-dom": "16.2.0",
                                            "**/react": "16.2.0",
                                            "**/typescript": "2.7.2",
                                            "**/ts-loader": "4.0",
                                            "**/webpack": "4.0.1",
                                            "**/webpack-dev-server": "3.1.0"
                                        }
                                    }, undefined, 3)}
                                    </pre>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export class Code extends React.Component {
    render() {
        return (
            <span className={styles.code}>{this.props.children}</span>
        )
    }
}