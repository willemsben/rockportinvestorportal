import { React } from "@yahara/react-spa/dist/externals";
import { bind } from "bind-decorator";

interface IProjectsPageProps {

}

interface IProjectsPageState {

}

export class ProjectsPage extends React.Component<IProjectsPageProps, IProjectsPageState> {
    state = {
        
    }

    render() {
        return (
            <div>
                <h2>Project List</h2>
                <p>
                    Herein lies a brief description of each project in the template solution, and it's
                    intended purpose.
                </p>
            </div>
        )
    }
}