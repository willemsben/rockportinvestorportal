import { React } from "@yahara/react-spa/dist/externals";
import { DependencyGrid } from "../components/DependencyGrid";
import { Terminal } from "../components/Terminal";
import { bind } from "bind-decorator";

const styles = require('./GettingStarted.scss') as {
    code: string;
    inset: string;
    main: string;
    table: string;
}


interface IGettingStartedPageState {
    username: string;
    project: string;
    customer: string;
}

export class GettingStartedPage extends React.Component<{}, IGettingStartedPageState> {
    state = {
        username: 'jrichardson',
        project: 'SuperCoolWebPortal',
        customer: 'AwesomeCustomer'
    } as IGettingStartedPageState;

    @bind
    handleCustomerUpdate(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            customer: e.target.value
        });
    }

    @bind
    handleProjectUpdate(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            project: e.target.value
        });
    }

    @bind
    handleUsernameUpdate(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            username: e.target.value
        });
    }

    render() {
        let username = this.state.username.replace(/\s+/g, '');
        let customer = this.state.customer.replace(/\s+/g, '');
        let project = this.state.project.replace(/\s+/g, '');

        let clientServiceDir = `C:\\Users\\${username}\\Desktop\\GitHub\\${project}\\ClientService`;

        return (
            <div className={styles.main}>
                <h3>Fill In Values</h3>
                <p>
                    This documentation is dynamic.  You can fill in the following values to get more accurate command examples, etc.
                </p>

                <div className={styles.inset}>
                    <table className={styles.table}>
                        <thead>
                            <tr>
                                <th>Field Name</th>
                                <th>Description</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Username</td>
                                <td>Your email address, without the <strong>@yaharasoftware.com</strong> part.</td>
                                <td>
                                    <input
                                        value={username}
                                        onChange={this.handleUsernameUpdate}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>Customer Name</td>
                                <td>The name of the customer this project is for (no spaces).</td>
                                <td>
                                    <input
                                        value={customer}
                                        onChange={this.handleCustomerUpdate}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>Project Name</td>
                                <td>The name of the project (no spaces).</td>
                                <td>
                                    <input
                                        value={project}
                                        onChange={this.handleProjectUpdate}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h3>Prerequisites</h3>
                <p>
                    You'll need the following before getting started:
                </p>

                <DependencyGrid />

                <p>
                    You'll also need to configure yarn and npm to look at Yahara's Proget repository for all <strong>@yahara</strong> packages,
                    by running the following commands:
                </p>

                <div className={styles.inset}>
                    <Terminal commands={[
                        {
                            pwd: clientServiceDir,
                            command: 'npm config set @yahara:registry https://proget.yaharasoftware.com/npm/defaultnpm/',
                        },
                        {
                            pwd: clientServiceDir,
                            command: 'npm login --registry=https://proget.yaharasoftware.com/npm/defaultnpm/ --scope=@yahara',
                            output: (colors) => {
                                return (
                                    <span>
                                        <span>Username: ({username})</span>
                                        <br/>
                                        <span>Password: (or leave unchanged)</span>
                                        <br/>
                                        <span>Email: (this IS public) ({username}@yaharasoftware.com)</span>
                                        <br/>
                                        <span>Logged in as {username} to scope @yahara on https://proget.yaharasoftware.com/npm/defaultnpm/.</span>
                                    </span>
                                )
                            },
                        },
                    ]}/>
                </div>

                <h3>Fork the Repo</h3>
                <ul>
                    <li>
                        Fork the <a href="https://bitbucket.org/yaharasoftware/react-spa-template">template</a> repo and checkout your new fork.
                    </li>
                    <li>
                        Rename the solution file to <span className={styles.code}>{project}.sln</span>.
                    </li>
                </ul>


                <h3>Replace the Core Library Source Code with Packages</h3>
                Open the project in visual studio and remove the following projects:
                <ul>
                    <li>Website.ASP.NET</li>
                    <li>Yahara.Core.Web</li>
                    <li>Yahara.Core.Web.AspNET</li>
                    <li>Yahara.Core.Web.AspNetCore</li>
                </ul>

                <p>
                    Using Nuget, install the following packages to the following projects.  Close Visual Studio when you're done.
                </p>
                <div className={styles.inset}>
                    <table className={styles.table}>
                        <thead>
                            <tr>
                                <th>Nuget Package</th>
                                <th>Project(s)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Yahara.Core.Web</td>
                                <td><ul>
                                    <li>ClientService</li>
                                    <li>Website.Core</li>
                                </ul></td>
                            </tr>
                            <tr>
                                <td>Yahara.Core.Web.AspNetCore</td>
                                <td><ul>
                                    <li>Website.Core</li>
                                </ul></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <p>
                    Delete the following directories:
                </p>
                <ul>
                    {[
                        'Website.ASP.NET\\',
                        'Yahara.Core.Web\\',
                        'Yahara.Core.Web.AspNET\\',
                        'Yahara.Core.Web.AspNetCore\\',
                        'ClientService\\submodules',
                        'ClientService\\node_modules',
                    ].map((name, i) => {
                        return <li key={i}><span className={styles.code}>{name}</span></li>
                    })}
                </ul>

                <p>Change your working directory to <span className={styles.code}>ClientService/</span> and run the following lines from the command line:</p>
                <div className={styles.inset}>
                    <Terminal commands={[
                        {
                            pwd: clientServiceDir,
                            command: 'yarn add @yahara/logging @yahara/react-spa @yahara/react-spa-builder',
                        },
                    ]}/>
                </div>

                <h3>Update the Root Namespace</h3>
                <p>
                    The root namespace in the example project is <span className={styles.code}>Yahara.WebCore.Example</span>.  By convention,
                    projects at Yahara use a root namespace comprised of the project name and customer name.
                </p>
                <p>
                    Open the solution directory in VSCode and do a global search and replace to replace all instances
                    of <span className={styles.code}>Yahara.WebCore.Example</span> with 
                    <span className={styles.code}>{customer}.{project}</span>.
                    Close VSCode when you're done.
                </p>

                <h3>Open and start the project</h3>
                <p>
                    Open the main solution with Visual Studio.  This is where you will update the server code.  You should be able 
                    to build and run the <strong>Web.Core</strong> project.
                </p>

                <p>
                    Open the <span className={styles.code}>ClientService</span> directory with VSCode.  This is where you will edit the client code.
                </p>

                <p>
                    From the command line, go to the <span className={styles.code}>ClientService</span> directory and run the
                    following command to start the hot reload server:
                </p>
                <div className={styles.inset}>
                    <Terminal commands={[
                        {
                            pwd: clientServiceDir,
                            command: '.\\node_modules\\.bin\\webpack-dev-server.cmd',
                        },
                    ]}/>
                </div>

                <p>
                    When creating a production package, run the following command before packaging up the application:
                </p>
                <div className={styles.inset}>
                    <Terminal commands={[
                        {
                            pwd: clientServiceDir,
                            command: '.\\node_modules\\.bin\\gulp compile-production',
                        },
                    ]}/>
                </div>

                <p>
                    Fun fact: this website is actually generated by running through the steps
                    on this page, and creating an IIS deploy package from the 
                    <span className={styles.code}>Web.Core</span> project.
                </p>
            </div>
        )
    }
}
