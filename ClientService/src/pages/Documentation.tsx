import { React } from "@yahara/react-spa/dist/externals";
import { bind } from "bind-decorator";
import { Link } from "react-router-dom";
import {
    Route,
    RouteComponentProps,
    Redirect
} from "react-router";
import { ComponentTemplatePage } from "./Documentation/ComponentTemplate";
import { TableOfContents } from "./Documentation/TableOfContents";
import { ProjectsPage } from "./Documentation/Projects";
import { DocumentationPages } from "./Documentation/documentationPages";
import * as _ from "lodash";
import { IApiOptions } from "../utilities/api";

const path = require('path');

const styles = require('./Documentation.scss') as {
    main: string;
    mainPanel: string;
    sidePanel: string;
}

interface IDocumentationPageProps {
    pageRoot: string;
    apiOptions: IApiOptions;
}

export class DocumentationPage extends React.Component<IDocumentationPageProps, {}> {
    @bind
    mkLink(address: string, text?: string) {
        if (!text) {
            text = address;
        }

        let url = path.join(this.props.pageRoot, address)

        return (
            <Link to={url}>{text}</Link>
        )
    }

    @bind
    route(address: string, render: (props: RouteComponentProps<any>) => JSX.Element) {
        let url = path.join(this.props.pageRoot, address);
        return (
            <Route
                key={url}
                path={url}
                render={render}
            />
        )
    }

    render() {
        return (
            <div className={styles.main}>
                <div className={styles.sidePanel}>
                    <TableOfContents
                        basePath={this.props.pageRoot}
                    />
                </div>

                <div className={styles.mainPanel}>
                    {_.map(DocumentationPages, (val, key) => {
                        return this.route(key, props => {
                            return val({
                                baseUrl: props.match.path,
                                apiOptions: this.props.apiOptions,
                            });
                        })
                    })}

                    <Route
                        path=''
                        exact={true}
                        render={(props) => {
                            return <Redirect to={path.join(this.props.pageRoot, 'Introduction')} />
                        }}
                    />
                </div>
            </div>
        )
    }
}
