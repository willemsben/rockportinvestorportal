import { React } from "@yahara/react-spa/dist/externals";
import { Link } from "react-router-dom";

const styles = require('./Home.scss') as {
    main: string;
}

export class HomePage extends React.Component<{}, any> {
    render() {
        return (
            <div className={styles.main}>
                <h2>Welcome</h2>
                <p>
                    The Yahara Web Framework consists of a collection of C# and Typescript
                    libraries distributed through NuGet and npm packages via 
                    Yahara's <a href='https://proget.yaharasoftware.com/'>Proget Server</a>.
                </p>

                <p>
                    The repository that backs this site is configured to be a reasonable starting
                    point and example application for new websites.  For instructions on how to 
                    use it to stand up your new web application, see 
                    the <Link to='/getting-started'>Getting Started</Link> page.
                </p>

                <p>
                    For more details on how the application works, please check out 
                    the <Link to='/documentation'>Documentation</Link> pages.
                </p>
            </div>
        )
    }
}
