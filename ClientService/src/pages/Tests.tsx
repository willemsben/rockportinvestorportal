import {React} from "@yahara/react-spa/dist/externals";

export class TestsPage extends React.Component<{}, any> {
    render() {
        return (
            <div>
                <h4>{Modernizr.flexbox ? "You do support flexbox" : "You don't support flexbox"}</h4>
                <h4>{(Modernizr as any)['cssgrid'] ? "You do support grid" : "You don't support grid"}</h4>

                <p className={`gogol`}>If you change this text, it'll update without a refresh!</p>
            </div>
        )
    }
}
