import { HomePage } from "./pages/Home";
import { TestsPage } from "./pages/Tests";
import {
    React, 
    BrowserRouter as Router,
    Route,
    Link
} from "@yahara/react-spa/dist/externals";
import { ISinglePageApplicationProps } from "@yahara/react-spa";
import { getLogger } from "@yahara/logging";
import { GettingStartedPage } from "./pages/GettingStarted";
import { DocumentationPage } from "./pages/Documentation";
import { AppToolbar } from "./components/AppToolbar";
import { IApiOptions } from "./utilities/api";

declare var require: {
    <T>(path: string): T;
    (paths: string[], callback: (...modules: any[]) => void): void;
    ensure: (paths: string[], callback: (require: <T>(path: string) => T) => void) => void;
};

// Include the global styles
require('./css/global.scss');

const styles = require('./Application.scss') as {
    main: string;
    page: string;
    pageOuter: string;
    toolbar: string;
}
const logger = getLogger('Application');

const logo = require("./images/logo.png") as string;
const logoFavicon = require('./images/favicon.ico') as string;

export class Application extends React.Component<ISinglePageApplicationProps, {}> {
    componentDidMount() {
        //console.log('Rendered version: ', this.props.getVersion());
    }
    
    render() {
        this.props.setFaviconUrl(logoFavicon);

        logger.info('Rendering application');

        let apiOptions: IApiOptions = {
            baseUrl: this.props.apiBaseUrl,
            wrappedFetch: fetch,
        };

        return (
            <Router>
                <div className={styles.main}>
                    <div className={styles.toolbar}>
                        <AppToolbar />
                    </div>

                    <div className={styles.pageOuter}>
                        <div className={styles.page}>
                            <Route path="/" exact render={props => {
                                this.props.setPageTitle('Home');
                                return (
                                    <HomePage />
                                );
                            }}/>

                            <Route path="/documentation" render={props => {
                                this.props.setPageTitle('Documentation');
                                return (
                                    <DocumentationPage
                                        pageRoot={props.match.path}
                                        apiOptions={apiOptions}
                                    />
                                );
                            }}/>

                            <Route path="/tests" exact render={props => {
                                this.props.setPageTitle('Tests');
                                return (
                                    <TestsPage />
                                )
                            }}/>

                            <Route path="/getting-started" exact render={props => {
                                this.props.setPageTitle('Getting Started');
                                return (
                                    <GettingStartedPage />
                                )
                            }}/>
                        </div>
                    </div>
                </div>
            </Router>
        )
    }
}
