import {React} from "@yahara/react-spa/dist/externals";

const styles = require('./DependencyGrid.scss') as {
    header: string;
    table: string;
};

const cmderLogo = require('../images/cmder-logo.png');
const nodeLogo = require('../images/nodejs-logo.png');
const vsCodeLogo = require('../images/vscode-logo.png');
const vsLogo = require('../images/vs-logo.png');
const yarnLogo = require('../images/yarn-logo.png');

export class DependencyGrid extends React.Component<{}, {}> {
    render() {
        return (
            <table className={styles.table}>
                <thead>
                    <tr className={styles.header}>
                        <th></th>
                        <th>Name</th>
                        <th>Minimum Version</th>
                        <th>Notes</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <img src={vsLogo}/>
                        </td>
                        <td>Visual Studio</td>
                        <td>2017 15.3+</td>
                        <td>
                            You'll also need to configure the Yahara Proget as a NuGet
                            repository for Visual Studio.  Refer to the email that
                            Adam sent out for more info.
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <a href="https://github.com/dotnet/core/blob/master/release-notes/download-archives/2.0.0-download.md">
                                .NET Standard SDK
                            </a>
                        </td>
                        <td>2.0</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <img src={vsCodeLogo}/>
                        </td>
                        <td>VS Code</td>
                        <td>1.15.1+</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <img src={nodeLogo}/>
                        </td>
                        <td>NodeJS</td>
                        <td>6</td>
                        <td>
                            Use the installer from the NodeJS 
                            <a href='https://nodejs.org/en/'> home page</a> to install this on Windows.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src={yarnLogo}/>
                        </td>
                        <td>Yarn</td>
                        <td>1.2.1</td>
                        <td>
                            Use the <a href='https://yarnpkg.com/en/docs/install'>official installer</a> for this as well.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src={cmderLogo}/>
                        </td>
                        <td>
                            <a href="http://cmder.net/">
                                Cmder
                            </a>
                        </td>
                        <td>N/A</td>
                        <td>
                            This one isn't strictly necessary.  You could theoretically use any windows command prompt, but
                            this is what I use, so I can be sure it'll always work for the commands in this documentation.
                        </td>
                    </tr>
                </tbody>
            </table>
        );
    }
}