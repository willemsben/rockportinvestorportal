import * as React from "react";

export interface IColors {
    darkGrey: string;
    green: string;
    grey: string;
    main: string;
    red: string;
    white: string;
    yellow: string;
};

const styles = require('./Terminal.scss') as IColors;

export interface ICommand {
    pwd: string;
    branch?: string;
    command: string;
    output?: (colorClassNames: IColors) => JSX.Element;
}

export interface ITerminalProps {
    commands: ICommand[];
}

export class Terminal extends React.Component<ITerminalProps, {}> {
    render() {
        let defaultPrompt = "λ";

        return (
            <pre className={styles.main}>
                {this.props.commands.map((cmd, i) => {
                    return (
                        <div key={i}>
                            <span className={styles.green}>{cmd.pwd}</span>
                            {cmd.branch && <span className={styles.red}> ({cmd.branch})</span>}
                            <br />

                            <span className={styles.darkGrey}>λ </span>
                            <span className={styles.grey}>{cmd.command}</span>
                            <br />

                            <span className={styles.grey}>
                                {cmd.output && cmd.output(styles)}
                            </span>

                            <br />
                        </div>
                    )
                })}
            </pre>
        )
    }
}