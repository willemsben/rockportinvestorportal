import * as React from "react";
import { Link, NavLink } from "react-router-dom";

const styles = require('./AppToolbar.scss') as {
    active: string;
    innerToolbar: string;
    link: string;
    logo: string;
    logoGroup: string;
    outerToolbar: string;
    titleText: string;
};

const logo = require("../images/logo.png") as string;

export class AppToolbar extends React.Component<{}, {}> {
    render() {
        return (
            <div className={styles.outerToolbar}>
                <div className={styles.innerToolbar}>
                    <span className={styles.logoGroup}>
                        <img
                            className={styles.logo}
                            src={logo}
                        />
                        <span className={styles.titleText}>Yahara Single Page Application</span>
                    </span>
                    
                    <span>
                        <NavLink
                            activeClassName={styles.active}
                            className={styles.link}
                            exact={true}
                            to="/">
                            Home
                        </NavLink>

                        <NavLink
                            activeClassName={styles.active}
                            className={styles.link}
                            exact={true}
                            to="/getting-started">
                            Getting Started
                        </NavLink>

                        <NavLink
                            activeClassName={styles.active}
                            className={styles.link}
                            to="/documentation">
                            Documentation
                        </NavLink>

                        <NavLink
                            activeClassName={styles.active}
                            className={styles.link}
                            exact={true}
                            to="/tests">
                            Basic Tests
                        </NavLink>
                    </span>
                </div>
            </div>
        )
    }
}