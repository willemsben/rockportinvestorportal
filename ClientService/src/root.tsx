import * as $ from 'jquery';

let baseUrl = `${$('base').attr('href')}/`

declare var __webpack_public_path__: string;
__webpack_public_path__ = baseUrl;

import {Application} from "./Application";
import {SPA} from "@yahara/react-spa";

let el = document.getElementById('reactApp');

let app = (el == null) ? new SPA(baseUrl) : new SPA(baseUrl, el);

app.setRootComponent(Application).render();


// When no module name is provided to module.hot.accept(), it assumes the current
// module: 
//   https://github.com/webpack/webpack/blob/7871b6b3dc32425406b3938be490a87aeb6fa794/lib/HotModuleReplacement.runtime.js#L89-L99
declare var module: any;
module.hot && module.hot.accept();