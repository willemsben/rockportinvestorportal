﻿const gulp = require('gulp');
const gulpWebpack = require('gulp-webpack');
const merge = require('webpack-merge');
const path = require('path');
const webpack = require('webpack');

gulp.task('compile-production', function () {
    process.env.NODE_ENV = 'production';

    const webpackConfig = require('./webpack.config');

    return gulp.src('./src/root.tsx')
        .pipe(gulpWebpack(webpackConfig, webpack))
        .pipe(gulp.dest('dist/'));
});

