var path = require('path');
const spaBuilder = require("@yahara/react-spa-builder");

var config = spaBuilder.generateWebpackConfig(path.resolve('./src/root.tsx'), {
    outDir: path.join(__dirname, 'dist'),
    devServerPort: 3030,
    proxy: 'http://localhost:53019',
    productionMode: process.env.NODE_ENV === 'production',
});

module.exports = config;
