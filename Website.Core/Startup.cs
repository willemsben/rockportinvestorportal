using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Yahara.Core.Web;
using Microsoft.Extensions.FileProviders;
using Rockport.InvestorPortal.Website.Core.Config;
using Yahara.Core.Web.AspNetCore;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;

namespace Website.Core
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            HostingEnvironment = env;

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get;  }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "API",
                    Version = "0.1.0",
                    Description = ""
                });

                c.DescribeAllEnumsAsStrings();

                var filePath = Path.Combine(AppContext.BaseDirectory, "Rockport.InvestorPortal.Website.Core.xml");
                c.IncludeXmlComments(filePath);
                //c.AddSecurityDefinition("Bearer", new ApiKeyScheme() { In = "header", Description = "Please insert JWT with Bearer into field", Name = "Authorization", Type = "apiKey" });
            });

            var singlePageApplicationType = typeof(SinglePageApplicationController);
            var assembly = typeof(SinglePageApplicationController).GetTypeInfo().Assembly;
            var baseNamespace = singlePageApplicationType.Namespace;

            var embeddedFileProvider = new EmbeddedFileProvider(assembly, baseNamespace);

            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.FileProviders.Add(embeddedFileProvider);
            });

            services.Configure<AppOptions>(Configuration.GetSection("appOptions"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute("DefaultApi", "api/{controller}/{action}/{id?}");

                // Single Page App
                routes.MapRoute("SPA", "{*path}", defaults: new { controller = "Spa", action = "Index" });
            });
        }
    }
}