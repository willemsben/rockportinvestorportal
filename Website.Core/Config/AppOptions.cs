using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rockport.InvestorPortal.Website.Core.Config
{
    public class AppOptions
    {
        public AppOptions()
        {

        }

        public string HotReloadServer { get; set; }
        public string ApiBaseUrl { get; set; }
    }
}