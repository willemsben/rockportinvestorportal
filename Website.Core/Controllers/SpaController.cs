using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yahara.Core.Web;
using Rockport.InvestorPortal.ClientService;
using Microsoft.Extensions.Configuration;
using Rockport.InvestorPortal.Website.Core.Config;
using Microsoft.Extensions.Options;
using Yahara.Core.Web.Interfaces;
using Yahara.Core.Web.AspNetCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Rockport.InvestorPortal.Website.Core.Controllers
{
    public class SpaController : SinglePageApplicationController
    {
        private string _hmrUrl = null;
        private string _apiBase = null;

        public SpaController(IOptions<AppOptions> appConfigAccessor)
        {
            _hmrUrl = appConfigAccessor.Value.HotReloadServer;
            _apiBase = appConfigAccessor.Value.ApiBaseUrl;
        }

        protected override bool AllowDevTools()
        {
            return true;
        }

        protected override string GetApiBaseUrl()
        {
            return _apiBase;
        }

        protected override ISinglePageApplicationFileService GetFileService()
        {
            return new FileService(_hmrUrl);
        }
    }
}