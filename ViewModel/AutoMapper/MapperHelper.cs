using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Rockport.InvestorPortal.DomainModel.Models;
using Rockport.InvestorPortal.ViewModel.Models;

namespace Rockport.InvestorPortal.ViewModel.AutoMapper
{
    /// <summary>
    /// This class provides static methods to generate AutoMapper configurations or instances.
    /// </summary>
    public class MapperHelper
    {
        // Cache the configuration, since we only need to generate it once.
        static MapperConfiguration config = null;

        static public MapperConfiguration GetConfiguration()
        {
            if (config == null)
            {
                config = new MapperConfiguration(cfg =>
                {
                    ApplyConfiguration(cfg);
                });
            }

            return config;
        }

        /// <summary>
        /// Applies a configuration to handle mapping between View Models and Domain Models
        /// objects.
        /// </summary>
        /// <param name="cfg">An AutoMapper IMapperConfigurationExpression</param>
        static public void ApplyConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<UserVM, User>().ReverseMap();

            return;
        }

        /// <summary>
        /// Get an IMapper capable of mapping ViewModel objects to DomainModel objects.
        /// </summary>
        /// <returns>IMapper instance for converting between Domain and View Models</returns>
        static public IMapper GetDataModelMapper()
        {
            return new Mapper(GetConfiguration());
        }
    }
}