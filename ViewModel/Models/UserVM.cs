using System;
using System.Collections.Generic;
using System.Text;

namespace Rockport.InvestorPortal.ViewModel.Models
{
    public class UserVM
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
    }
}